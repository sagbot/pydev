# Pydev

A CLI tool for running a local dockerized development environment

## Installing
```
pip install pydev
```

## Example usage
```
cd /my-project
pydev up
```

## Trobleshooting:
1) run ```pydev validate``` to make sure that everything is installed properly
2) purge all running containers by running ```pydev purge```