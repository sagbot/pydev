import sys
from argparse import _SubParsersAction as SubParsers
import logging

import colorama

from pydev.docker import docker_works, docker_compose_works

def validate(plugins_dir: str, *args, **kwargs):
    sys.stdout.write(f'Testing that docker is installed...   ')
    if not docker_works():
        print(colorama.Fore.RED + 'Failed')
        print('Docker does not seem to be installed & working, terminating')
        exit(1)
    print(colorama.Fore.GREEN + 'Done')

    sys.stdout.write(f'Testing that docker-compose is installed...   ')
    if not docker_compose_works():
        print(colorama.Fore.RED + 'Failed')
        print('Docker compose does not seem to be installed and working, terminating')
        exit(1)
    print(colorama.Fore.GREEN + 'Done')

def add_parser(subparsers: SubParsers) -> None:
    validate_parser = subparsers.add_parser('validate', help='Validate that all required dependencies are installed')
    validate_parser.set_defaults(func=validate)