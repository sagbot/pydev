import sys

from pydev.docker import pruge_all_containers

from argparse import _SubParsersAction as SubParsers
import colorama

def purge(*args, **kwargs):
    sys.stdout.write(f'Purging old containers...   ')
    pruge_all_containers()
    print(colorama.Fore.GREEN + 'Done')

def add_parser(subparsers: SubParsers) -> None:
    purge_parser = subparsers.add_parser('purge', help='Purge all containers')
    purge_parser.set_defaults(func=purge)