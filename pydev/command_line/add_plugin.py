from argparse import _SubParsersAction as SubParsers

def add_plugin(plugins_dir: str, source: str, *args, **kwargs):
    print(f'Cloning {source} into {plugins_dir}')
    plugin_name = get_repo_name_from_url(source)
    git.Repo.clone_from(url=source, to_path=path.join(plugins_dir, plugin_name))


def add_parser(subparsers: SubParsers) -> None:
    add_plugin_parser = subparsers.add_parser('add-plugin', help='Add a new plugin from a git repo')
    add_plugin_parser.add_argument(dest='source', help='The git repository to add as a plugin')
    add_plugin_parser.set_defaults(func=add_plugin)