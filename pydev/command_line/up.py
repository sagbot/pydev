import sys
import os
import logging
from argparse import _SubParsersAction as SubParsers

import colorama

from pydev.docker import docker_works, docker_compose_works
from pydev.plugins import get_plugins, stop_plugin, start_plugin

def up(plugins_dir: str, verify: bool = False, purge: bool = False, *args, **kwargs):
    if verify:
        if not docker_works():
            logging.error('Docker does not seem to be installed, terminating')
            exit(1)

        if not docker_compose_works():
            logging.error('Docker compose does not seem to be installed, terminating')
            exit(1)
    
    if purge:
        sys.stdout.write(f'Purging old containers...   ')
        pruge()
        print(colorama.Fore.GREEN + 'Done')

    logging.debug('Creating plugins dir at %s', plugins_dir)
    os.makedirs(plugins_dir, exist_ok=True)

    plugins = get_plugins(plugins_dir)
    logging.debug('Found %s plugins', len(plugins))

    for plugin in plugins:
        sys.stdout.write(f'Stopping {plugin.name}...   ')
        stop_plugin(plugin)
        print(colorama.Fore.GREEN + 'Done')
        logging.debug('Plugin %s stopped', plugin.name)
    
    for plugin in plugins:
        sys.stdout.write(f'Starting {plugin.name}...   ')
        start_plugin(plugin)
        print(colorama.Fore.GREEN + 'Done')
    

    print('pydev is up and running!')
    for plugin in plugins:
        if plugin.url:
            print(f'\t{plugin.name} - {plugin.url}')
        else:
            print(f'\t{plugin.name}')
        
        if plugin.description:
            print(f'\t\t{plugin.description}')

def add_parser(subparsers: SubParsers) -> None:
    up_parser = subparsers.add_parser('up', help='Start the development environment')
    up_parser.add_argument('--no-verify',
                           action='store_false',
                           dest='verify',
                           default=True,
                           help='If specified the installation of docker & docker-compose will not be verified')
    up_parser.add_argument('--purge',
                           action='store_true',
                           dest='purge',
                           default=False,
                           help='Purge all running & stopped containers')
    up_parser.set_defaults(func=up)