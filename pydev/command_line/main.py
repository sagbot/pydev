import argparse
from os import path
from pathlib import Path
import logging

import colorama

from . import (
  up, 
  validate, 
  purge, 
  add_plugin
)

def get_parser():
    colorama.init(autoreset=True)

    parser = argparse.ArgumentParser()
    argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter,
                            prog='pydev',
                            description='''
A tool for deploying a local containerised python development environment, with a 
full web interface''',
                                     epilog='''
Notes:
  * You need to have docker installed (You can use Docker for windows as well)
  * You need to install docker-compose
                                     '''
                                     )

    parser.add_argument('-p', '--plugins-dir',
                        dest='plugins_dir',
                        type=str,
                        help='The path to the pydev plugins directory, containing docker-compose files',
                        required=False,
                        default=path.join(Path.home(), '.pydev'))
    
    parser.add_argument('-l', '--log-level',
                        dest='log_level',
                        choices=['DEBUG', 'INFO', 'WARN', 'ERROR', 'CRITICAL'],
                        default='INFO',
                        help='Sets the log level')

    subparsers = parser.add_subparsers(help='sub-command help')

    up.add_parser(subparsers)
    validate.add_parser(subparsers)
    purge.add_parser(subparsers)
    add_plugin.add_parser(subparsers)

    return parser

def main():
    colorama.init(autoreset=True)

    parser = get_parser()
    args = parser.parse_args()

    # TODO: Make sure that the args have a log level argument

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=getattr(logging, args.log_level))

    logging.debug(f'args: {vars(args)}')

    # TODO: Make sure that hte args have a func argument

    args.func(**vars(args))
