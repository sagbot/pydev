import os
import logging
import subprocess
import glob
from os import path
import re
from typing import List

import git
from pydantic import BaseModel

class Plugin(BaseModel):
    path: str
    name: str
    url: str = None
    description: str = None


def get_context_info() -> dict:
    return {
        'PYDEV_PROJECT_ROOT': os.getcwd()
    }


def stop_plugin(plugin: Plugin):
    logging.debug(f'Shutting down plugin: {plugin.name}')
    env = {**os.environ.copy(), **get_context_info()}
    process = subprocess.run(f"docker-compose -f {plugin.path} down --remove-orphans",
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             env=env)
    
    logging.debug('shutdown complete with return code: %s', process.returncode)

    if process.returncode != 0:
        logging.debug('stdout: %s', process.stdout)
        logging.error(process.stderr)


def start_plugin(plugin: Plugin):
    logging.debug(f'Starting plugin: {plugin.name}')
    env = {**os.environ.copy(), **get_context_info()}
    process = subprocess.run(f"docker-compose -f {plugin.path} up -d",
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             env=env)
    
    logging.debug('start complete with return code: %s', process.returncode)

    if process.returncode != 0:
        logging.debug('stdout: %s', process.stdout)
        logging.error(process.stderr)


def get_compose_file_from_dir(dir: str) -> str:
    docker_compose_path = path.join(dir, 'docker-compose.yaml')
    if not path.isfile(docker_compose_path):
        docker_compose_path = path.join(dir, 'docker-compose.yml')
        
    if not path.isfile(docker_compose_path):
        raise Exception('no docker compose file found in {}'.format(dir))

    return docker_compose_path


def get_plugins(plugins_dir: str) -> List[Plugin]:
    logging.debug('Searching for plugins in %s', plugins_dir)
    
    plugins = []
    for name in os.listdir(plugins_dir):
        docker_compose_path = get_compose_file_from_dir(path.join(plugins_dir, name))

        comment_lines = []
        with open(docker_compose_path) as f:
            for line in f:
                if line.startswith('#'):
                    comment_lines.append(line)
                else:
                    break
        
        url = None
        description = None

        for line in comment_lines:
            matches = re.findall(r'^\s*#\s*url:\s*(.*?)\s*$', line)
            if matches and len(matches) == 1:
                url = matches[0]
            
            matches = re.findall(r'^\s*#\s*description:\s*(.*?)\s*$', line)
            if matches and len(matches) == 1:
                description = matches[0]

        plugins.append(Plugin(
            name=name,
            path=docker_compose_path,
            url=url,
            description=description
        ))
    
    return plugins


def add_git_plugin(plugins_dir: str, source: str, *args, **kwargs):
    plugin_name = get_git_repo_name_from_url(source)
    git.Repo.clone_from(url=source, to_path=path.join(plugins_dir, plugin_name))


def get_git_repo_name_from_url(url: str) -> str:
    last_slash_index = url.rfind("/")
    last_suffix_index = url.rfind(".git")
    if last_suffix_index < 0:
        last_suffix_index = len(url)

    if last_slash_index < 0 or last_suffix_index <= last_slash_index:
        raise Exception("Badly formatted url {}".format(url))

    return url[last_slash_index + 1:last_suffix_index]
