import logging
import subprocess

import docker

def docker_works() -> bool:
    logging.debug('running docker ps to make sure docker is installed')
    process = subprocess.run("docker ps",
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
    logging.debug(f'docker ps return code: %s', process.returncode)

    return process.returncode == 0

def docker_compose_works() -> bool:
    logging.debug('running docker-compose --help to make sure it is installed')
    process = subprocess.run("docker-compose --help",
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
    logging.debug(f'docker-compose --help return code: %s', process.returncode)

    return process.returncode == 0

def pruge_all_containers():
    docker_client = docker.from_env()
    docker_client.containers.prune()
    containers = docker_client.containers.list()
    for container in containers:
        logging.debug(f'purging container {container.name}, status: {container.status}')
        if container.status == 'running':
            container.stop()
        
        container.remove()
