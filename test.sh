#!/bin/sh

echo "testing pydev --help"
pydev --help

echo "testing pydev validate with debug log level"
pydev --log-level DEBUG validate

echo "testing pydev validate without log level"
pydev validate

echo "Adding mockit plugin from git repo"
pydev add-plugin https://github.com/boyney123/mockit.git

echo "Starting pydev env"
pydev up