from pydev.plugins import get_git_repo_name_from_url

def test_http_git_repo_name_1():
    repo = 'https://github.com/keycloak/keycloak.git'
    name = get_git_repo_name_from_url(repo)
    expected_name = 'keycloak'
    assert name == expected_name

def test_http_git_repo_name_2():
    repo = 'https://github.com/git/git.git'
    name = get_git_repo_name_from_url(repo)
    expected_name = 'git'
    assert name == expected_name

def test_ssh_git_repo_name_1():
    repo = 'git@github.com:keycloak/keycloak.git'
    name = get_git_repo_name_from_url(repo)
    expected_name = 'keycloak'
    assert name == expected_name

def test_ssh_git_repo_name_2():
    repo = 'git@github.com:git/git.git'
    name = get_git_repo_name_from_url(repo)
    expected_name = 'git'
    assert name == expected_name